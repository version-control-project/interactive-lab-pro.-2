﻿//For player movement 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
	[SerializeField] 
	private float m_JumpForce = 400f;							/// The force appleid to the jump.
	
	[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;			/// The maxspeed for crouch
	
	[Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;	         ///Smoothing out the character movement
	
	[SerializeField] private bool m_AirControl = false;							/// Seeing if player can move while in the air
	[SerializeField] private LayerMask m_WhatIsGround;							
	[SerializeField] private Transform m_GroundCheck;							
	[SerializeField] private Transform m_CeilingCheck;							
	[SerializeField] private Collider2D m_CrouchDisableCollider;				/// Collider that will be disabled when crouching

	const float k_GroundedRadius = .2f; 
	private bool m_Grounded;                                /// Seeing whenever player is grounded or not
	const float k_CeilingRadius = .2f; 
	private Rigidbody2D m_Rigidbody2D;
	private bool m_FacingRight = true;                     /// Seeing whenever player is facing left or right 
	private Vector3 m_Velocity = Vector3.zero;

	[Header("Events")]
	[Space]

	public UnityEvent OnLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }

	public BoolEvent OnCrouchEvent;
	private bool m_wasCrouching = false;

	public int maxHealth = 4;
	public int currentHealth;

	public HealthBar healthBar;

	bool hasBeenHit = false;

    public Transform pivotpoint;

	public Transform crosshair;

    Vector3 mouseWorldPosition;
    Vector3 mouseDirection;

    float mouseAngle;

	public GameObject MenuGameOver;

	private void Awake()
	{
		m_Rigidbody2D = GetComponent<Rigidbody2D>();         	/// Gets rigibody component

		if (OnLandEvent == null)
			OnLandEvent = new UnityEvent();

		if (OnCrouchEvent == null)
			OnCrouchEvent = new BoolEvent();
	}

	void Start()
	{
		currentHealth = maxHealth;              /// Identifying that the current health (health bar) is in the max health
		healthBar.SetMaxHealth(maxHealth);
	}

	void Update()                                                                         /// For moving the hand - using math to set the angles, etc
    {
		mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		crosshair.position = mouseWorldPosition + new Vector3 (0, 0, 0.3f); ;
        mouseDirection = mouseWorldPosition - transform.position;
		
        mouseAngle = Mathf.Atan2(mouseDirection.y, mouseDirection.x) * Mathf.Rad2Deg;
        pivotpoint.eulerAngles = new Vector3(0, 0, mouseAngle);                              /// Makes it so the arm rotates from the parent (pivot point) of the hand 
    }

	void OnTriggerEnter2D(Collider2D other)              /// collision detection with monster
	{
		if(other.transform.name == "MonsterHit" && hasBeenHit == false) 
		{
  			TakeDamage(1);
  			hasBeenHit = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.transform.name == "MonsterHit") 
		{
  			hasBeenHit = false;
		}
	}
	void TakeDamage(int damage)
	{
		currentHealth -= damage;

		healthBar.SetHealth(currentHealth);

		if (currentHealth == 0)
		{
			MenuGameOver.SendMessage("GameOver");
		}
	}

	private void FixedUpdate()
	{
		bool wasGrounded = m_Grounded;
		m_Grounded = false;

		/// The player is grounded - check to see if circle is overlaping 
		Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				m_Grounded = true;
				if (!wasGrounded)
					OnLandEvent.Invoke();
			}
		}
	}

	public void Move(float move, bool crouch, bool jump)
	{
		if (!crouch)                                                             /// If crouching, check to see if the character can stand up - keep them crouching 
		{
			if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
			{
				crouch = true;
			}
		}
		                                  
		if (m_Grounded || m_AirControl)                              ///Only control the player when grounded 
		{
			if (crouch)
			{
				if (!m_wasCrouching)
				{
					m_wasCrouching = true;
					OnCrouchEvent.Invoke(true);
				}

				move *= m_CrouchSpeed;                                     /// Reduce the speed while crouching

				if (m_CrouchDisableCollider != null)                        /// Disable one of the colliders when crouching
					m_CrouchDisableCollider.enabled = false;
			} else
			{
				if (m_CrouchDisableCollider != null)                        /// Enable the collider when not crouching
					m_CrouchDisableCollider.enabled = true;

				if (m_wasCrouching)
				{
					m_wasCrouching = false;
					OnCrouchEvent.Invoke(false);
				}
			}

			Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);                  /// Move the character 
			m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);   	/// Smoothing the movement out

			if (move > 0 && !m_FacingRight)        /// For moving right when charatcre is facing left - flips
			{
				Flip();
			}
			else if (move < 0 && m_FacingRight) 			/// For moving left when character is facing right - flips
			{
				Flip();
			}
		}
		if (m_Grounded && jump)		            /// Player jump input (checking to see if the character is grounded first)
		{
			m_Grounded = false;                                      /// Add a force to the plauer's jump
			m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
		}
	}

	private void Flip()
	{
		m_FacingRight = !m_FacingRight;     /// Switch the way the player is facing
		///pivotpoint.position = !pivotpoint.position;  ///not gonna use

		Vector3 theScale = transform.localScale;         		/// Multiply the player's x localScale by -1
		theScale.x *= -1;
		transform.localScale = theScale;

	}
}
