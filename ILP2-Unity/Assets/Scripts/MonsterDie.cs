﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterDie : MonoBehaviour
{

    private object gameobject;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(gameObject);
    }
}