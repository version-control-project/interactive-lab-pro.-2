﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpScale : MonoBehaviour
{

    public float maxSize;
    public float minSize;

    Vector3 maxScale;
    Vector3 minScale;

    public float growDuration;
    public float shrinkDuration;

    public bool isGrowing;

    float startTimer;
    public AnimationCurve animationCurve;

    // Use this for initialization
    void Start() {
        
        maxScale = new Vector3(maxSize, maxSize, 1);                  /// Create Vector3s for both the min and max sizes
        minScale = new Vector3(minSize, minSize, 1);
        transform.localScale = minScale;                       /// set the scale of the object to the minimum scale
        isGrowing = true;                      /// initially, the object should grow

        ResetTimer();
    }

    void ResetTimer() {
        startTimer = Time.time;
    }

    void Grow() {
        
        float timeElapsed = Time.time - startTimer;                     /// calculate the time elapsed since the timer was started
 
        float percent = timeElapsed / growDuration;                   /// get the percent of time elapsed in relation to the grow duration

        if (percent > 1) {                            /// if the timer has elapsed, switch states
            transform.localScale = maxScale;
            isGrowing = false;
            ResetTimer();
            return;
        }

        // calculate the current scale of the square using the percent and animation curve
        Vector3 newScale = Vector3.Lerp(minScale, maxScale, animationCurve.Evaluate(percent));
        transform.localScale = newScale;
    }


    void Shrink() {

        float percent = (Time.time - startTimer) / shrinkDuration;             /// get the percent of time elapsed in relation to the shrink duration

        if (percent > 1) {                                  /// if the timer has elapsed, switch states
            transform.localScale = minScale;
            isGrowing = true;
            ResetTimer();
            return;
        }

        // calculate the current scale of the square using the percent and animation curve
        Vector3 newScale = Vector3.Lerp(maxScale, minScale, animationCurve.Evaluate(percent));
        transform.localScale = newScale;
    }

    void Update() {                                    /// Update is called once per frame
        if (isGrowing) {
            Grow();
        } else if (!isGrowing) {
            Shrink();
        }
    }
}

