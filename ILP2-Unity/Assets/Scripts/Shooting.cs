﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;

    // Update is called once per frame
    void Update()
    {
        // if the mouse is pressed exicute "shoot"
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    //Instantiates the bullet to spawn at the tip of the gun
    void Shoot()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation); 
    }
}
