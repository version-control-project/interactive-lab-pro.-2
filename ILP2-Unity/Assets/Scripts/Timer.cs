﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;

    void Start()                  /// Start is called before the first frame update
    {
        startTime = Time.time;            /// sets the start time
    }

    void Update()         /// Update is called once per frame
    {
 
        float t = Time.time - startTime;                        /// float for time 
        string minutes = ((int)t / 60).ToString();              /// coverts minutes to a string of numbers
        string seconds = (t % 60).ToString("f2");               /// coverts seconds to a string of numbers 

        timerText.text = minutes + ":" + seconds;

    }
}

