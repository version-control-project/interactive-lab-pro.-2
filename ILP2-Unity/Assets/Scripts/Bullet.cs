﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Sets variables
    public float speed = 20f;
    public Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        // multiplys the forces of the bullet to the speed allowing the bullet to move
        rb.velocity = transform.right * speed;
    }
}

